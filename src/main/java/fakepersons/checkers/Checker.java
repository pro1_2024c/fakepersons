package fakepersons.checkers;

import fakepersons.Utils;
import fakepersons.data.InputItem;
import fakepersons.data.OutputItem;
import java.io.IOException;
import java.util.Properties;

public abstract class Checker
{
    public abstract OutputItem[] Check(InputItem[] input);

    public abstract String GetCheckerName();

    public void CheckAndPublish(InputItem[] input, Properties config)
    {
        OutputItem[] outputItems = Check(input);
        System.out.println("Check done with "+ outputItems.length+" results ("+GetCheckerName()+")");

        try
        {
            String response = Utils.PublishOutputs(outputItems, GetCheckerName(), config);
            System.out.println("Server response "+ response+" ("+GetCheckerName()+")");
        }
        catch (InterruptedException e)
        {
            System.out.println("Publishing interrupted: "+ e.getMessage());
        }
        catch (IOException e)
        {
            System.out.println("Publishing failed: "+ e.getMessage());
        }
    }
}

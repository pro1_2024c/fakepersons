package fakepersons.customCheckers;

import fakepersons.checkers.Checker;
import fakepersons.data.InputItem;
import fakepersons.data.OutputItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DuplicateChecker extends Checker
{
    @Override
    public OutputItem[] Check(InputItem[] input)
    {
        HashMap<String, InputItem> hashMap = new HashMap<>();
        List<OutputItem> result = new ArrayList<>();
        for (int i=0; i<input.length; i++)
        {
            InputItem item = input[i];
            String key = item.getIdCardNumber();
            if(hashMap.containsKey(key))
            {
                InputItem old = hashMap.get(key);
                String message = "Toto číslo dokladu už má "+
                        old.getFirstName() + " "+old.getSurname();
                result.add(new OutputItem(i,message));
            }
            else
            {
                if(!item.getIdCardNumber().isBlank())
                {
                    hashMap.put(key, item);
                }
            }
        }

        return result.toArray(new OutputItem[0]);
    }

    @Override
    public String GetCheckerName() {
        return "Kontrola unikátních čísel";
    }
}

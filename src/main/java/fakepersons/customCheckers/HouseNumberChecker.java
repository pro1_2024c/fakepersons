package fakepersons.customCheckers;

import fakepersons.checkers.OneByOneChecker;
import fakepersons.data.InputItem;

public class HouseNumberChecker extends OneByOneChecker
{
    @Override
    public String GetCheckerName()
    {
        return "Číslo domu";
    }

    @Override
    public String CheckSingleItem(InputItem inputItem)
    {
        for(int i=0; i< inputItem.getHouseNumber().length(); i++)
        {
            char ch = inputItem.getHouseNumber().charAt(i);
            if(!Character.isDigit(ch))
            {
                return "Nečíselná hodnota: "+inputItem.getHouseNumber();
            }
        }
        return null;
    }
}
